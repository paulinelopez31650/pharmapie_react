
import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import ajoutPharmacie from './components/ajouterpharmacie';
import PharmacieGarde from './components/pharmaciegarde';
import PharmaList from './components/pharmalist';
import Welcome from './welcome';
import Footer from './footer';

class App extends Component {
  render() {
    return (
    <Router>
        <div>
            <Welcome/>
            <nav className="text-center navigation">
                <ul className="flex-wrap md:flex flex-row justify-center uppercase">
                    <p className="btn_link shadow-md"><Link to={'/pharmalist'} className="font-bold">Les pharmacies</Link></p>
                    <p className="btn_link shadow-md"><Link to={'/pharmaciegarde'} className="font-bold">La pharmacie de garde du jour</Link></p>
                     <p className="btn_link shadow-md"><Link to={'/ajouterpharmacie'} className="font-bold">Ajouter une pharmacie</Link></p>
                </ul>
            </nav>
          <hr />
          <Switch>
              <Route exact path='/pharmalist' component={PharmaList} />
              <Route exact path='/pharmaciegarde' component={PharmacieGarde} />
              <Route exact path='/ajouterpharmacie' component={ajoutPharmacie} />
          </Switch>
          <Footer/>
        </div>
      </Router>
    );
  }
}


export default App;
