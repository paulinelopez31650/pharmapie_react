
import React from 'react';
import axios from "axios";
import PharmaList from './pharmalist.js';


class PharmaEdit extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            garde: '',
            nom: '',
            ville: '',
            quartier: '',
            edit: false
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        axios.get(`https://floating-atoll-16404.herokuapp.com/pharma`)
            .then(res => {
                res.data.map(pharmacies => {
                    {
                        pharmacies.id === this.props.id &&
                        this.setState({
                            nom: pharmacies.nom,
                            quartier: pharmacies.quartier,
                            ville: pharmacies.ville,
                            garde: pharmacies.garde
                        })
                    };
                });
            });
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        axios.put('https://floating-atoll-16404.herokuapp.com/pharma' + this.props.id, {
            nom: this.state.nom,
            garde: this.state.garde,
            ville: this.state.ville,
            quartier: this.state.quartier
        })
            .catch(function (error) {
                console.log(error)
            })
            .then(() => {
                alert('Modification ok pour : ' + this.state.nom)
            })
    }

    render() {
        return (
            <div>
                {this.state.edit === false &&
                    <>
                        <form onSubmit={this.handleSubmit}>
                            <div class="mb-3">
                                <label>
                                    Nom: <input type="text" name="nom" value={this.state.nom} onChange={this.handleChange} />
                                </label>
                            </div>
                            <div class="mb-3">
                                <label>
                                    Ville: <input type="text" name="ville" value={this.state.ville} onChange={this.handleChange} />
                                </label>
                            </div>
                            <div class="mb-3">
                                <label>
                                    Quartier: <input type="text" name="quartier" value={this.state.quartier} onChange={this.handleChange} />
                                </label>
                            </div>
                            <div class="mb-3" >
                                <label>
                                    Jour de garde :
                        <select value={this.state.garde} name="garde" onChange={this.handleChange} >
                                        <option value="lundi">Lundi</option>
                                        <option value="mardi">Mardi</option>
                                        <option value="mercredi">Mercredi</option>
                                        <option value="jeudi">Jeudi</option>
                                        <option value="vendredi">Vendredi</option>
                                        <option value="samedi">Samedi</option>
                                        <option value="dimanche">Dimanche</option>
                                    </select>
                                </label>
                            </div>
                            <input type="submit" value="" />
                        </form>
                    </>
                }
                {this.state.edit === true && <PharmaList />}
            </div>
        );
    }
}

export default PharmaEdit;
