import axios from 'axios';
import React from 'react';
import PharmaEdit from './editerpharmacie.js';

class PharmaList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pharma: [],
      edit : false,
      idPharma : 0
    }
  }
  componentDidMount() {
    axios.get('https://floating-atoll-16404.herokuapp.com/pharma')
      .then(res => {
        const pharma = res.data;
        this.setState({ pharma: pharma });
      })
  }

  deleteRow(id, e) {
    axios.delete(`https://floating-atoll-16404.herokuapp.com/pharma/${id}`)
      .then(res => {
        console.log(res);
        console.log(res.data);

        const pharma = this.state.pharma.filter(pharma => pharma.id !== id);
        this.setState({ pharma });
      })
  }

    editPharma(id, nom){
    this.setState({modify: true});
    axios.put('https://floating-atoll-16404.herokuapp.com/pharma/${id}')
          .then(()=> console.log(`La pharmacie ${id} à été modifiée`), alert(`${nom} modifiée`))
          .catch(function (error) {
            console.log(error);
          });
  }

  render() {
    return (
      <div>
        {this.state.edit === false &&
          <><ul>
            {
                this.state.pharma.map((pharma, index) =>
                  <div>
                    <h4 className="titre_pharma"key={index}>{pharma.nom}</h4>
                    <p key={index}>{pharma.quartier}</p>
                    <p key={index}>{pharma.ville}</p>
                    <td>
                      <button className="btn btn-danger" onClick={(e) => this.deleteRow(pharma.id, e)}>Supprimer</button>
                    </td>
                    <td>
                      <button className="btn btn-success" title="edit" onClick={() => this.setState({ edit: true, idPharma: pharma.id })}>Editer</button>
                    </td>
                  </div>
              )}
            </ul></>
        }
        {this.state.edit === true &&
          <PharmaEdit id={this.state.idPharma} />}
      </div>
    )
  }

}
export default PharmaList;

