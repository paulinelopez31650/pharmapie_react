import React, { Component } from 'react';
import axios from 'axios';

class ajoutPharmacie extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nom: 'Pharmacie ...',
      quartier: '',
      ville: '',
      garde: 'mardi',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    axios.post('https://floating-atoll-16404.herokuapp.com/pharma', {
      nom: this.state.nom,
      garde: this.state.garde,
      ville: this.state.ville,
      quartier: this.state.quartier
    });
    alert("Ok pour l'ajout de la " + this.state.nom);
  }

  render() {
    return (
      <div class="text-center">
        <h4 className="ajout_pharma">Ajouter une pharmacie</h4>
        <form onSubmit={this.handleSubmit}>
          <div class="mb-3">
            <label class="form-label"> Nom de la pharmacie :
               <input type="text" class="form-control" name="nom" value={this.state.nom} onChange={this.handleChange} />
            </label>
          </div>
          <div class="mb-3">
            <label class="form-label">Quartier :
               <input type="text" class="form-control" name="quartier" value={this.state.quartier} onChange={this.handleChange} />
            </label>
          </div>
          <div class="mb-3">
            <label class="form-label">La ville :
               <input type="text" class="form-control" name="ville" value={this.state.ville} onChange={this.handleChange} />
            </label>
          </div>
          <div class="mb-3">
            <label class="form-label">Le jour de garde:
            <select value={this.state.day} class="form-control" name="garde" onChange={this.handleChange}>
              <option value="lundi">Lundi</option>
              <option value="mardi">Mardi</option>
              <option value="mercredi">Mercredi</option>
              <option value="jeudi">Jeudi</option>
              <option value="vendredi">Vendredi</option>
              <option value="samedi">Samedi</option>
              <option value="dimanche">Dimanche</option>
            </select>
            </label>
          </div>
          <input type="submit" value="Envoyer" />
        </form>
      </div>
    )
  }
}


export default ajoutPharmacie;