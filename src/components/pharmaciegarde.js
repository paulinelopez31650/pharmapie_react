import React, { Component } from 'react';
import axios from 'axios';

class PharmacieGarde extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pharma: []
        }
    }
    componentDidMount() {
        axios.get("https://floating-atoll-16404.herokuapp.com/pharma-garde")
            .then(res => {
                const pharma = res.data;
                this.setState({pharma:pharma});
            })
    }

    render() {
        return (
            <div>
                <h4 class="text-center" className="pharma_garde">La pharmacie de garde aujourd'hui</h4>
            {
                this.state.pharma.length ? 
                <ul>
                    {this.state.pharma.map((pharma, index) =>
                    <div>
                        <h4 key={index}>{pharma.nom}</h4>
                        <p key={index}>{pharma.quartier}</p>
                        <p key={index}>{pharma.ville}</p>
                    </div>
                    )}
                </ul> :
                <div>Loading pharma list...</div>
            }
            </div>
        )
    }

}

export default PharmacieGarde;