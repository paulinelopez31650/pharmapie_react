import React from 'react';
import logo from './logo.jpg';

class Welcome extends React.Component {
  render() {
return <div class="container welcome">
  <div class="row">
    <div class="col-sm">
  <h1 className="text-center">Pharmapie</h1>
    </div>
    <div class="col-sm">
  <img src={logo} alt="Logo" />
    </div>
  </div>
</div>

  }
}
export default Welcome;